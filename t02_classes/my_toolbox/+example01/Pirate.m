classdef Pirate < example01.AdvancedHuman
  
  methods
    function obj = Pirate(name)
      obj = obj@example01.AdvancedHuman(name);
    end %function
    
    function hello(obj)
      disp('AAARRRRGGGHHHH');
    end %function
  end %methods
  
end

